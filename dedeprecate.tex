\documentclass[article,a4paper,]{memoir}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{color}
\usepackage{ulem}
\usepackage[american]
           {babel}        % needed for iso dates
\usepackage[iso,american]
           {isodate}      % use iso format for dates
\usepackage{listings}
\lstset{language=C++}
\usepackage{url}
\usepackage{hyperref}
\hypersetup{
	colorlinks=true,
	urlcolor=blue,
	citecolor=black,
	linkcolor=black
}




% From the standard tex
%%--------------------------------------------------
%% Difference markups
\definecolor{addclr}{rgb}{0,.6,.6}
\definecolor{remclr}{rgb}{1,0,0}
\definecolor{noteclr}{rgb}{0,0,1}

\renewcommand{\added}[1]{\textcolor{addclr}{\uline{#1}}}
\newcommand{\removed}[1]{\textcolor{remclr}{\sout{#1}}}
\renewcommand{\changed}[2]{\removed{#1}\added{#2}}

\newcommand{\nbc}[1]{[#1]\ }
\newcommand{\addednb}[2]{\added{\nbc{#1}#2}}
\newcommand{\removednb}[2]{\removed{\nbc{#1}#2}}
\newcommand{\changednb}[3]{\removednb{#1}{#2}\added{#3}}
\newcommand{\remitem}[1]{\item\removed{#1}}

\newcommand{\ednote}[1]{\textcolor{noteclr}{[Editor's note: #1] }}
% \newcommand{\ednote}[1]{}

\linespread{1.25}
\setlrmarginsandblock{3.5cm}{2.5cm}{*}
\setulmarginsandblock{2.5cm}{*}{1}
\checkandfixthelayout 

\lstset{
morecomment=*[l][\color{red}]{//-},
morecomment=**[l][\color{green}]{//+},
moredelim=**[is][\color{addclr}]{@}{@}
}

\title{De-deprecating volatile compound operations}
\author{Paul M. Bendixen <paulbendixen@gmail.com>}
\date{2021-09-15}
\begin{document}
%\maketitle

\begin{tabular}{ll}
\textbf{Number:} & P2327R1 \\
\textbf{Title:} & \huge{\thetitle} \\
\textbf{Project:} & ISO JTC1/SC22/WG21: Programming Language C++ \\
\textbf{Audience:} & SG14, SG1, SG22, EWG, WG21 \\
\textbf{Date:} & \printdate{\thedate} \\
\textbf{Authors:} & \theauthor \\
\textbf{Contributors:} & Jens Maurer \\
			& Arthur O'Dwyer \\
			& Ben Saks \\
\textbf{Email:} & \href{mailto:paulbendixen@gmail.com}{paulbendixen@gmail.com} \\
\textbf{Reply to:} & Paul M. Bendixen \\
\end{tabular}
\chapter*{Revision history}
\section*{Changes R0 $\to$ R1}
Focus on bitwise compound expression solution, drop general dedeprecaion of compound expressions.

\chapter{Introduction}
The C++ 20 standard deprecated many functionalities of the volatile keyword. This was due to P1152\cite{P1152R4}.
The reasoning is given in the R0 version of the paper\cite{P1152R0}.

The deprecation was not received too well in the embedded community as volatile is commonly used for communicating with 
peripheral devices in microcontrollers\cite{Wouter20}.

The purpose of this paper is to give a solution that will not undo what was achieved with P1152, and still 
keep the parts that are critical to the embedded community.

\chapter{Problem statement}
\section{Background}
One of the great advantages of C++ is its closeness to the machine on which it operates.
This enables C++ to be used in very constrained devices such as microcontrollers without any operating system.
These systems often operate by doing bitwise manipulation on memory mapped registers.

While there are multiple ways to do bitwise manipulation in a memory location the most idiomatic way is something along 
the following lines:

\begin{lstlisting}
// In vendor supplied hardware abstraction layer
struct ADC {
	volatile uint8_t CTRL;
	volatile uint8_t VALUE;
	...
};
#define ADC_ENABLE (1 << ADC_CTRL_ENABLE)


// in user code
ADC1->CTRL |= ADC_ENABLE; // Start the ADC
...
ADC1->CTRL &= ~ADC_ENABLE; // Stop the ADC
\end{lstlisting}

Vendors supply hardware abstraction libraries (HALs) containing macros or inline functions for setting or clearing bits
utilizing this idiom, or may use it in code created by code generators.
The following example is from a library for the Energy Micro (now Silicon Labs) series of ARM microcontrollers:

\begin{lstlisting}
// Copyright 2012 Energy Micro EMLib from em_i2c.h
__STATIC_INLINE void I2C_IntDisable(I2C_TypeDef *i2c, uint32_t flags)
{
  i2c->IEN &= ~(flags);
}
\end{lstlisting}


It is clear from the previous example that this idiomatic usage of compound operations clashes with the deprecation of
compound operations on volatile. The vendors of these libraries are almost always the chip vendors themselves and they
supply C libraries that are used by C++ developers. Based on experience they will  not be likely to update their header
files to conform to the C++ standard.
The argument against compound operations on volatile variables is that it leads the
programmer to believe that the operation itself becomes compounded and therefore atomic.

Now since there can be a read, modify, write cycle it is possible that an interrupt would happen in
between the read and the write, however most embedded code that uses this idiom needs to take care of this by being
right by construction i.e. not bit-twiddling variables that are used in the interrupt service routines (ISRs).
The problem being that there is currently \emph{no} way to do this in a guaranteed atomic manner (see also
\cite[section 3.4.1]{P2268}).

The fact that the code must be correct by construction makes it seem that C++20 is deprecating fully functioning code.

\section{Scope}
So what is the impact of deprecating compound operations on volatile?
A search of some of the more widely used embedded libraries show that, it is in some way used
in all hardware libraries commonly used for embedded systems available today.

The numbers below are done by using UNIX grep\footnote{The search was done using the command
\\\texttt{grep -re "[A-Z\_0-9]\textbackslash{}+\textbackslash{}(\textbackslash{}[.*\textbackslash{}]\textbackslash{})*[ ]\textbackslash{}+[\&|]=" --include \textbackslash{}*.h .}}
to find usages of either \lstinline{|=} or \lstinline{&=} on variables with a code style usually associated with
volatile members  in the code bases of the respective libraries.
While this method isn't foolproof it does give an indication of the problem.

\begin{table}[hb]
	\centering
\begin{tabular}{|l|r|}
	\hline
	Library & compound operations \\
	\hline
	\hline
Silabs Gecko SDK & 293 \\
AVR libc 2.0 & 205 \\
Raspberry pi Pico SDK & 13\\
\hline
\end{tabular}
\caption{Occurrences of compound operations on variables typically associated with volatile}
\end{table}

This illustrates the major point, not only is there a lot of code already written out there \emph{working as expected}
but the usage of this idiom also prevents adopters of C++ to use the C libraries provided with their toolchains.

Furthermore, one of the most common ways to gradually change from C to C++ is by keeping the same code and compile it
with a C++ compiler. (See e.g. \cite[from 27:46]{embo}). This procedure involves modifying the code to be correct in 
C++ and would possibly involve moving compound volatile statements to non-compound statements. However the prevalence
of this idiom in vendor supplied C headers would prevent this low cost approach and would require a larger up front 
investment creating new headers to replace the vendor supplied ones.

The idea that businesses will be willing to spend the effort to update existing codebases that become defunct because of
these changes seems unlikely, rather it seems likely that they will mandate using no newer standard versions.

\section{Error potential}
One of the arguments for deprecation in \cite{P1152R0} is the potential for error for programmers unaccustomed to using
volatile. However removal of the compound operators will also risk the introduction of errors such as in the following.

\begin{lstlisting}
UART1->UCSR0B |= (1<<UCSZ01); // (1)
UART1->UCSR0B = UART1->UCSR0B | (1<<UCSZ01); // (2)
UART2->UCSR0B = UART1->UCSR0B | (1<<UCSZ01); // (3)
\end{lstlisting}

The code in (1) is the original code, setting a bit in a mapped register. In (2) the code is transformed to the style
that is suggested as a replacement. (3) describes a possible error scenario, where the device is changed to another,
but due to the code duplication of the updated style, an error has slipped in and the value of the old device is read.

An error such as the above will not necessarily be caught in code review, and will possibly not even be found in
immediate testing if \lstinline{UART1} happens to have the correct setting during testing, such code is also notoriously
hard to unit test. As such the deprecation will trade errors where volatile is erroneously used to express atomicity for
hard to discover and hard to detect errors due to duplication.

\section{Didn't we just go over this?}
While the proposal to deprecate was heard in committee meetings, the main focus was on problems arising with 
multi-threaded code (SG1) and with EWG, neither of these groups can be expected to be familiar with the inner workings
on microcontrollers.

\chapter{Solution}
As the compound operations are mainly used for bitwise manipulation, the proposed solution is to only de-deprecate the use of bitwise
compound operations (\texttt{ |= \&= \^{}= }) as these are the ones that are useful for this purpose. 
In the examination of the libraries \texttt{+=} and \texttt{-=} did not occur, however they might in commercial / closed source libraries.

\chapter{Wording}
Change expr.ass paragraph 6 
\begin{quote}
The behavior of an expression of the form E1 op= E2 is equivalent to E1 = E1 op E2 except that E1 is evaluated only once.
Such expressions are deprecated if E1 has volatile-qualified type\added{ and op is not one of the bitwise operators \texttt{|}, \texttt{\&}, \texttt{\^}}; see [depr.volatile.type].
For += and -=, E1 shall either have arithmetic type or be a pointer to a possibly cv-qualified completely-defined object type.
In all other cases, E1 shall have arithmetic type.
\end{quote}

Change expr.ass paragraph 5
\begin{quote}
	\changed{A simple}{An} assignment whose left operand is of a volatile-qualified type is deprecated ([depr.volatile.type]) unless the (possibly parenthesized) assignment is a discarded-value expression or an unevaluated operand.
\end{quote}

%Add context to add this line, because the += should stay
Add the following to the examples in [depr.volatile.type] paragraph 2
\begin{lstlisting}
	@brachiosaur |= neck; // OK Bitwise compound expression
\end{lstlisting}

\chapter{Impact}
The changes proposed by this paper would affect the current proposal P2139\cite{P2139}.
\chapter{Thanks}
Thanks to Jens Maurer for the suggestion on the solution, for wording and presentation assistance.\\
Thanks to Wouter van Ooijen for starting this discussion.\\
Thanks to the entire SG14 group for feedback on initial drafts.

\bibliography{dedeprecate}
\bibliographystyle{apalike}
\end{document}

